# Totpex

## TOTP: Time-Based One-Time Password written in Elixir

TOTP client.

### How to use it

````bash
Totpex.generate_totp("mysecretkey_base32encoded")
"798396"

Totpex.validate_totp("mysecretkey_base32encoded", "234556")
true | false
````

**As a standalone application**

````bash
# build binary (one time operation)
mix escript.build

# Execute application
./totpex
Please enter your secret key: 634hszwpdilkzqe2
Your One-Time Password is 382765
````

### Thank you

* kyleaa - https://gitlab.com/kyleaa
* jameslafa - https://twitter.com/jameslafa
* Herman verschooten - https://gitlab.com/Hermanverschooten
