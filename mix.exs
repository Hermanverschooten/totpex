defmodule Totpex.Mixfile do
  use Mix.Project

  def project do
    [
      app: :totpex,
      version: "0.1.4",
      elixir: "~> 1.5",
      build_embedded: Mix.env() == :prod,
      start_permanent: Mix.env() == :prod,
      description: description(),
      package: package(),
      deps: deps(),
      name: "Totpex",
      source_url: "https://gitlab.com/ahamtech/elixir/totpex.git"
    ]
  end

  def application do
    [extra_applications: [:logger]]
  end

  defp escript_config do
    [main_module: Totpex.CLI]
  end

  defp deps do
    [
      {:ex_doc, ">= 0.0.0", only: :dev},
      {:credo, "~> 1.3", only: [:dev, :test], runtime: false}
    ]
  end

  defp description() do
    "
    TOTP: Time-Based One-Time Password written in Elixir
    "
  end

  defp package() do
    [
      name: "totpex",
      files: ["lib", "mix.exs", "README*", "LICENSE*"],
      maintainers: ["Anwesh Reddy", "Mahesh Reddy"],
      licenses: ["GNU Lesser General Public License v3.0"],
      links: %{"Gitlab" => "https://gitlab.com/ahamtech/elixir/totpex.git"}
    ]
  end
end
